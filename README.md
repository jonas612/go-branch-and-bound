# Branch & Bound in Go

## Installation
Follow these steps to install all requirements to run this programm.
1. Install [Go](https://golang.org/)

### Contribution
These steps are only required when you want to make contributions to this project.

1. Install [Pre commit](https://pre-commit.com/#install)
1. Navigate to the project directory
1. Run `pre-commit install`

## Execution
### Setup
Change the variables in the `main class to your likings. The following parameters can be configured.
- `runCount` Determines how many times the algorithm is executed sequentially for the given problem. You will receive a summary over all runs in the end
- `generateSample` Offers the possibility to generate a new TSP problem. Valid values are `true`, `false`
- `sampleSize` Size of the automatically generated sample. The TSP cost matrix will have the dimension [`sampleSize` x `sampleSize`].
- `sampleLimit` Maximum value for costs between locations.
- `threadCount` Determines the number of workers to use. (Is only eveluated in combination with `mode = branchandbound.ModeParallel`)
- `mode` Determines the mode of operation. Valid values are
   - `branchandbound.ModeParallel` Parallel execution with `threadCount` workers
   - `branchandbound.ModeSerial` Very simple serial approach using depth-first search and recursion
- `boundMode` Determines how the lower bounds are calculated. Valid values are:
   - `branchandbound.SimpleLowerBound` Simple method proposed in [[Wiener 2003]](http://www.jot.fm/contents/issue_2003_05/column7.html)
   - `branchandbound.ComplexLowerBound` Lower bound calculation with incoming and outgoing edges + restrictions by the tour
- `printLogs` Describes whether the algorithm should print logs (e.g. what worker calculates what node). With `false` only results are printed. Valid values `true`, `false`. Make sure to turn off `printLogs` for greater calculations to not flood you terminal. This can also slow down the calculation.

In addition to the above configuration you can also configure the load balancing mode directly in `/branchandbound/thread.go:17`
- `BalancingModeNone` does not perform any load balancing
- `BalancingModeSimple` request-based load balancing with a single node in response
- `BalancingModeAdvanced` request-based load balancing with a half the nodes in response

The algorithm comes with a variety of predefined samples. Samples are stored in `/samples/samples.go` and can be selected in `main.go:48`.


### Execution
Run the following commands to execute the code.
1. `cd /path/to/the/repository`
2. `go build .`
3. `./go-branch-and-bound`

It is also possible to use IDEs with Go support (e.g. IntelliJ) for execution. However, you should consider the possible overhead in these environments when benchmarking.

The output should look like the following:
```
./go-branch-and-bound                                      
[MAIN] === Start Run 0 ===

=== SUMMARY Run 0 ===
Duration:          169.198µs
Nodes explored:    12
Nodes explored (per Thread): map[0:3 1:4 2:5]
Optimal Tour:      [0 2 3 1 0]
Optimal tour cost: 80
Final lower bound: 80.000000

[MAIN] === Start Run 1 ===

=== SUMMARY Run 1 ===
Duration:          150.286µs
Nodes explored:    16
Nodes explored (per Thread): map[0:4 1:5 2:7]
Optimal Tour:      [0 2 3 1 0]
Optimal tour cost: 80
Final lower bound: 80.000000

[MAIN] === Start Run 2 ===

=== SUMMARY Run 2 ===
Duration:          92.537µs
Nodes explored:    16
Nodes explored (per Thread): map[0:5 1:4 2:7]
Optimal Tour:      [0 1 3 2 0]
Optimal tour cost: 80
Final lower bound: 80.000000

[MAIN] === Start Run 3 ===

=== SUMMARY Run 3 ===
Duration:          60.348µs
Nodes explored:    12
Nodes explored (per Thread): map[0:4 1:3 2:5]
Optimal Tour:      [0 1 3 2 0]
Optimal tour cost: 80
Final lower bound: 80.000000

[MAIN] === Start Run 4 ===

=== SUMMARY Run 4 ===
Duration:          59.055µs
Nodes explored:    16
Nodes explored (per Thread): map[0:4 1:5 2:7]
Optimal Tour:      [0 2 3 1 0]
Optimal tour cost: 80
Final lower bound: 80.000000


=== FINAL SUMMARY ===
Threads:		3
Number of runs:		5
Av. Duration:		106.000000  (59.055µs - 169.198µs)
Av. Nodes explored:	14.400000  (12 - 16)
Total Duration:		530
Total Nodes explored:	72

Thread usages:
R\T 	0	1	2
0	3	4	5
1	4	5	7
2	5	4	7
3	4	3	5
4	4	5	7
```

## Benchmarking
### Run the Benchmark
Go includes extensive benchmarking features. For easy comparison of the different implementation an automated benchmark script is provided.

To start the benchmarking process run
```bash
go test -run=Bench -bench .  -benchtime=10s ./...
```

The algorithm is delivered with a variety of samples. Thus, running the whole Benchmark will take a lot of time. You can specify the sample you want to benchmark by providing its name.
```bash
# This will only run the benchmark for the 24Cities sample
go test -run=Bench -bench=24Cities ./...

# This will only run the benchmark for all 16CitiesAlt1 samples
go test -run=Bench -bench=16CitiesAlt1 ./...

# This will run the benchmark for all 16Cities samples
go test -run=Bench -bench=16Cities ./...
```

### Interpret the Output
When you execute the benchmark, you will receive a similar output like the following (the results were generated with a Ryzen 3700X@3.6Ghz [ 8Cores | 16 Threads ]).

```
go test -bench=16Cities -benchtime=100x -timeout 600m -run=^$ ./...                                                                                                                               
goos: linux
goarch: amd64
pkg: gitlab.com/jonas612/go-branch-and-bound/branchandbound
BenchmarkTSP_Solve_S_1_16Cities-16         	     100	11489448364 ns/op
BenchmarkTSP_Solve_P_1_16Cities-16         	     100	5119914777 ns/op
BenchmarkTSP_Solve_P_2_16Cities-16         	     100	2175453710 ns/op
BenchmarkTSP_Solve_P_3_16Cities-16         	     100	1520650322 ns/op
BenchmarkTSP_Solve_P_4_16Cities-16         	     100	1124040773 ns/op
BenchmarkTSP_Solve_P_5_16Cities-16         	     100	 915720393 ns/op
BenchmarkTSP_Solve_P_6_16Cities-16         	     100	 833759117 ns/op
BenchmarkTSP_Solve_P_7_16Cities-16         	     100	 750934480 ns/op
BenchmarkTSP_Solve_P_8_16Cities-16         	     100	 717778521 ns/op
BenchmarkTSP_Solve_P_9_16Cities-16         	     100	 725294029 ns/op
BenchmarkTSP_Solve_P_10_16Cities-16        	     100	 724902467 ns/op
BenchmarkTSP_Solve_S_1_16CitiesAlt1-16     	     100	2389701210 ns/op
BenchmarkTSP_Solve_P_1_16CitiesAlt1-16     	     100	 281158503 ns/op
BenchmarkTSP_Solve_P_2_16CitiesAlt1-16     	     100	 175388457 ns/op
BenchmarkTSP_Solve_P_3_16CitiesAlt1-16     	     100	 138009234 ns/op
BenchmarkTSP_Solve_P_4_16CitiesAlt1-16     	     100	 112147211 ns/op
BenchmarkTSP_Solve_P_5_16CitiesAlt1-16     	     100	  91379746 ns/op
BenchmarkTSP_Solve_P_6_16CitiesAlt1-16     	     100	  83739054 ns/op
BenchmarkTSP_Solve_P_7_16CitiesAlt1-16     	     100	  76710340 ns/op
BenchmarkTSP_Solve_P_8_16CitiesAlt1-16     	     100	  82393436 ns/op
BenchmarkTSP_Solve_P_9_16CitiesAlt1-16     	     100	  79292529 ns/op
BenchmarkTSP_Solve_P_10_16CitiesAlt1-16    	     100	  75189161 ns/op
BenchmarkTSP_Solve_S_1_16CitiesAlt2-16     	     100	24225752989 ns/op
BenchmarkTSP_Solve_P_1_16CitiesAlt2-16     	     100	11812221043 ns/op
BenchmarkTSP_Solve_P_2_16CitiesAlt2-16     	     100	7126244282 ns/op
BenchmarkTSP_Solve_P_3_16CitiesAlt2-16     	     100	5385231414 ns/op
BenchmarkTSP_Solve_P_4_16CitiesAlt2-16     	     100	4376783399 ns/op
BenchmarkTSP_Solve_P_5_16CitiesAlt2-16     	     100	3668625810 ns/op
BenchmarkTSP_Solve_P_6_16CitiesAlt2-16     	     100	3343509328 ns/op
BenchmarkTSP_Solve_P_7_16CitiesAlt2-16     	     100	3118002228 ns/op
BenchmarkTSP_Solve_P_8_16CitiesAlt2-16     	     100	3005744077 ns/op
BenchmarkTSP_Solve_P_9_16CitiesAlt2-16     	     100	3033235338 ns/op
BenchmarkTSP_Solve_P_10_16CitiesAlt2-16    	     100	2897819581 ns/op
BenchmarkTSP_Solve_S_1_16CitiesAlt3-16     	     100	3218110941 ns/op
BenchmarkTSP_Solve_P_1_16CitiesAlt3-16     	     100	 533289829 ns/op
BenchmarkTSP_Solve_P_2_16CitiesAlt3-16     	     100	 302580778 ns/op
BenchmarkTSP_Solve_P_3_16CitiesAlt3-16     	     100	 236567620 ns/op
BenchmarkTSP_Solve_P_4_16CitiesAlt3-16     	     100	 196243643 ns/op
BenchmarkTSP_Solve_P_5_16CitiesAlt3-16     	     100	 175794819 ns/op
BenchmarkTSP_Solve_P_6_16CitiesAlt3-16     	     100	 152484772 ns/op
BenchmarkTSP_Solve_P_7_16CitiesAlt3-16     	     100	 142292951 ns/op
BenchmarkTSP_Solve_P_8_16CitiesAlt3-16     	     100	 131733053 ns/op
BenchmarkTSP_Solve_P_9_16CitiesAlt3-16     	     100	 128424015 ns/op

[...]
```

Because the results strongly vary for different versions of the TSP problem the benchmark has fixed samples in different sizes to make them comparable. The naming convention for the benchmarking is "BenchmarkTSP_Solve_`mode`_`threadCount`_`sampleSize`-`numberOfSystemThreadsAvailable" with the parameters explained in "Custom Execution".

Given a time limit (here: `-benchtime=10s` but you might also use a different period) Go benchmark executes the TSP solver as many times as it requires to receive statistically stable results with respect to that time limit. The first number represents how many times the benchmark executed the TSP algorithm. The second value is the mean of executions times in `ns`. Especially for the greater sample sizes a higher value vor `-benchtime` would stabelize the test results, because more runs are possible. However, this of course increases the time the benchmark takes to complete. Alternatively, you can also select the specific number of run by using `-banchtime=Nx` with `N` being the number of runs. 

For more detailed information refer to the [Go Documentation](https://golang.org/pkg/testing/).
