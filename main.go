package main

import (
	"fmt"
	"time"

	"gitlab.com/jonas612/go-branch-and-bound/branchandbound"
	sample2 "gitlab.com/jonas612/go-branch-and-bound/samples"
)

// algorithm configuration
const (
	runCount       = 5
	sampleSize     = 16
	sampleLimit    = 20
	threadCount    = 3
	mode           = branchandbound.ModeParallel
	boundMode      = branchandbound.ComplexLowerBound
	printLogs      = true
	generateSample = false
)

var (
	durationsCache              = []time.Duration{}
	exploredNodesCache          = []int64{}
	exploredNodesPerThreadCache = []map[int]int{}
	costCache                   = []int64{}
)

func main() {
	var (
		tsp    *branchandbound.TSP
		sample map[string]map[string]int64
	)

	// generates a randomized sample for the test
	if generateSample {
		limit := int64(sampleLimit)
		sample = sample2.Generate(sampleSize, &limit)
		sample2.PrintGo(sample)
	}

	// executes the tsp as many times as defined in runCount and records metrics
	for i := 0; i < runCount; i++ {
		fmt.Printf("[MAIN] === Start Run %d ===\n", i)

		// EXAMPLE Selection
		tsp = branchandbound.NewTSP(sample2.Example4, "0", threadCount, printLogs)

		start := time.Now()

		tsp.Solve(mode, boundMode)

		duration := time.Since(start)

		durationsCache = append(durationsCache, duration)
		exploredNodesCache = append(exploredNodesCache, tsp.GetNodesExplored())
		exploredNodesPerThreadCache = append(exploredNodesPerThreadCache, tsp.GetNodesExploredPerThread())
		costCache = append(costCache, tsp.GetLowestCost())

		// print run results
		printRunResults(tsp, duration, i)
	}

	// calculates and prints final results
	printFinalResults()
}

func printRunResults(tsp *branchandbound.TSP, duration time.Duration, run int) {
	fmt.Printf("\n=== SUMMARY Run %d ===\n"+
		"Duration:          %v\n"+
		"Nodes explored:    %d\n"+
		"Nodes explored (per Thread): %v\n"+
		"Optimal Tour:      %v\n"+
		"Optimal tour cost: %d\n"+
		"Final lower bound: %f\n\n",
		run, duration, tsp.GetNodesExplored(), tsp.GetNodesExploredPerThread(), tsp.GetBestTour(), tsp.GetLowestCost(), tsp.GetLowestBound())
}

func printFinalResults() {
	var (
		exploredNodesSum int64 = 0
		exploredNodesMax       = exploredNodesCache[0]
		exploredNodesMin       = exploredNodesCache[0]
		durationsSum     int64 = 0
		durationsMax           = durationsCache[0]
		durationsMin           = durationsCache[0]
	)

	for _, value := range exploredNodesCache {
		exploredNodesSum += value
		if value < exploredNodesMin {
			exploredNodesMin = value
		}
		if value > exploredNodesMax {
			exploredNodesMax = value
		}
	}
	for _, value := range durationsCache {
		durationsSum += value.Microseconds()
		if value < durationsMin {
			durationsMin = value
		}
		if value > durationsMax {
			durationsMax = value
		}
	}
	exploredNodesAverage := float64(exploredNodesSum) / float64(runCount)
	durationsAverage := float64(durationsSum) / float64(runCount)

	fmt.Printf("\n=== FINAL SUMMARY ===\n"+
		"Threads:		%d\n"+
		"Number of runs:		%d\n"+
		"Av. Duration:		%f  (%v - %v)\n"+
		"Av. Nodes explored:	%f  (%d - %d)\n"+
		"Total Duration:		%v\n"+
		"Total Nodes explored:	%v\n\n"+
		"Thread usages: \n",
		threadCount, runCount,
		durationsAverage, durationsMin, durationsMax,
		exploredNodesAverage, exploredNodesMin, exploredNodesMax,
		durationsSum, exploredNodesSum)

	fmt.Printf("R\\T \t")
	for i := 0; i < threadCount; i++ {
		fmt.Printf("%d\t", i)
	}
	fmt.Println()
	for i := 0; i < runCount; i++ {
		fmt.Printf("%d\t", i)
		for j := 0; j < threadCount; j++ {
			fmt.Printf("%d\t", exploredNodesPerThreadCache[i][j])
		}
		fmt.Println()
	}

	// check if the final weight is qual for all runs
	for i := range costCache {
		if i == 0 {
			continue
		}
		if costCache[i-1] != costCache[i] {
			fmt.Println("ERROR: The results are not identically among the different runs!")
		}
	}
}
