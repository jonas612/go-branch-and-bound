package main

import (
	"testing"
)

func TestMain_Main(t *testing.T) {
	// executes the current program once to check
	// if it is runnable in the current configuration
	main()
}
