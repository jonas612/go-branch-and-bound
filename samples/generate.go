package samples

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

// Generate generates a TSP sample of a tsp problem of a given sizes
func Generate(size int, limit *int64) map[string]map[string]int64 {
	// default limit 100
	var randLimit int64= 100
	if limit != nil {
		randLimit = *limit
	}

	rand.Seed(time.Now().UnixNano())

	sample := map[string]map[string]int64{}
	for i := 0; i < size; i++ {
		sample[strconv.Itoa(i)] = map[string]int64{}
	}
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			if j > i {
				continue
			}
			if j == i {
				sample[strconv.Itoa(i)][strconv.Itoa(j)] = -1
				continue
			}
			random := rand.Int63n(randLimit)
			if random == 0 {
				random = 1
			}
			sample[strconv.Itoa(i)][strconv.Itoa(j)] = random
			sample[strconv.Itoa(j)][strconv.Itoa(i)] = random
		}
	}
	return sample
}

// Print prints a sample in a human readable way
func Print(sample map[string]map[string]int64) {
	fmt.Println("===SAMPLE===")
	for i := 0; i < len(sample); i++ {
		fmt.Print("[\t")
		for j := 0; j < len(sample); j++ {
			fmt.Printf("%d\t", sample[strconv.Itoa(i)][strconv.Itoa(j)])
		}
		fmt.Print("]\n")
	}
	fmt.Println()
}

// PrintGo prints a sample in go nested map style
func PrintGo(sample map[string]map[string]int64) {
	fmt.Println("===SAMPLE===")
	fmt.Println("map[string]map[string]int64{")
	for i := 0; i < len(sample); i++ {
		fmt.Printf("\"%d\":{\t", i)
		for j := 0; j < len(sample); j++ {
			fmt.Printf("\"%d\":%d,\t", j,sample[strconv.Itoa(i)][strconv.Itoa(j)])
		}
		fmt.Print("},\n")
	}
	fmt.Println("}")
}
