package branchandbound

import (
	"fmt"
)

// MaxInt64 represents the highest number possible in the int64 format
const MaxInt64 = 1<<63 - 1

// SimpleLowerBound represents config for simple lower bound calculation
const SimpleLowerBound = 0
// ComplexLowerBound represents config for complex lower bound calculation
const ComplexLowerBound = 1

// Node is a node in a branch and bound tree
type Node struct {
	Parent     *Node
	CostMatrix map[string]map[string]int64
	Tour       []string
	LowerBound float64
	TotalCost  int64
	Calculated bool
	boundMode int
}

// NewNode returns a new Node
func NewNode(tsp map[string]map[string]int64, parent *Node, tour []string, boundMode int) *Node {
	n := Node{}
	n.CostMatrix = tsp
	n.Parent = parent
	n.Tour = tour
	n.boundMode = boundMode
	return &n
}

// Calc calculates the values for a Node
func (n *Node) Calc() float64 {
	var lowerBound float64

	if n.boundMode == SimpleLowerBound {
		lowerBound = n.calcSimpleLowerBound()
	}
	if n.boundMode == ComplexLowerBound {
		lowerBound = n.calcComplexLowerBound()
	}


	n.LowerBound = lowerBound
	n.TotalCost = n.currentTotalCost()
	n.Calculated = true
	return lowerBound
}

// CreateChildren returns all possible child Nodes for a Node
func (n *Node) CreateChildren() []*Node {
	var children []*Node
	for destination := range n.CostMatrix[n.location()] {
		if !n.visited(destination) {
			newTour := make([]string, len(n.Tour))
			copy(newTour, n.Tour)
			newTour = append(newTour, destination)
			children = append(children, NewNode(
				n.CostMatrix,
				n,
				newTour,
				n.boundMode,
			))
		}
	}

	return children
}

// ToString prints the values of a Node in a readable format
func (n *Node) ToString() string {
	return fmt.Sprintf("[NODE] Tour: %s ; LB: %f, Weight: %d; Level: %d", n.Tour, n.LowerBound, n.TotalCost, n.Level())
}

// ToStringID prints a short version of the Nodes values
func (n *Node) ToStringID() string {
	return fmt.Sprintf("[NODE] Tour: %s(%d)", n.Tour, n.Level())
}

// Level return the current level of a Node in the tree
func (n *Node) Level() int {
	return len(n.Tour) - 1
}

func (n *Node) calcSimpleLowerBound() float64 {
	var lowerBound float64

	if n.Level() == 0 {
		for _, row := range n.CostMatrix {
			lowerBound += float64(minimum(row))
		}
	}

	if n.Level() > 0 {
		blocked := map[string]bool{}

		lowerBound += float64(n.currentTotalCost())

		for _, loc := range n.Tour {
			blocked[loc] = true
		}
		lowerBound += float64(n.minimumWithBlocked(n.CostMatrix[n.location()], blocked))
		blocked[n.startLocation()] = false
		for loc, row := range n.CostMatrix {
			if loc == n.startLocation() || blocked[loc] {
				continue
			}
			lowerBound += float64(n.minimumWithBlocked(row, blocked))
		}
	}

	return lowerBound
}

func (n *Node) calcComplexLowerBound() float64 {
	var lowerBound float64

	if n.Level() == 0 {
		var sum int64
		for _, row := range n.CostMatrix {
			min, min2 := minima(row)
			sum += min + min2
		}
		lowerBound = float64(sum) / 2
	}

	if n.Level() > 0 && n.Level() < len(n.CostMatrix) {
		var intermediateBound int64
		intermediateBound = 0
		intermediateBound += n.currentTotalCost() * 2                                          // accounts for all edges that are fixed in the tour
		intermediateBound += minimumToAdd(n.CostMatrix[n.startLocation()], n.secondLocation()) // accounts for starting location
		intermediateBound += minimumToAdd(n.CostMatrix[n.location()], n.lastLocation())        // accounts for current location
		intermediateBound += n.minimaOfRemainingLocations()                                    // accounts for the remaining locations
		lowerBound = float64(intermediateBound) / 2
	}

	if n.Level() == len(n.CostMatrix) {
		if !(n.location() == n.startLocation()) {
			panic("wrong final node!")
		}
		lowerBound = float64(n.currentTotalCost())
	}

	return lowerBound
}

func (n *Node) currentStepCost() int64 {
	if n.Level() == 0 {
		return 0
	}
	return n.CostMatrix[n.Tour[n.Level()-1]][n.Tour[n.Level()]]
}

func (n *Node) currentTotalCost() int64 {
	if n.Parent == nil {
		return n.currentStepCost()
	}
	return n.Parent.TotalCost + n.currentStepCost()
}

func (n *Node) visited(location string) bool {
	for _, stop := range n.Tour {
		if location == stop {
			return true
		}
	}
	return false
}

func (n *Node) location() string {
	return n.Tour[n.Level()]
}

func (n *Node) lastLocation() string {
	return n.Tour[n.Level()-1]
}

func (n *Node) startLocation() string {
	return n.Tour[0]
}

func (n *Node) secondLocation() string {
	return n.Tour[1]
}

func minimumToAdd(row map[string]int64, connectedLocation string) int64 {
	var min int64 = MaxInt64
	var min2 int64 = MaxInt64
	var minKey string
	for key, a := range row {
		if a == -1 {
			continue
		}
		if a < min {
			min = a
			minKey = key
			continue
		}
	}

	for key, a := range row {
		if a == -1 || key == minKey {
			continue
		}
		if a < min2 {
			min2 = a
			continue
		}
	}

	if connectedLocation == minKey {
		return min2
	}
	return min
}

func (n *Node) minimaOfRemainingLocations() int64 {
	var sum int64
	sum = 0
	for loc, row := range n.CostMatrix {
		if n.visited(loc) {
			continue
		}
		min, min2 := minima(row)
		sum += min + min2
	}
	return sum
}

func minima(row map[string]int64) (int64, int64) {

	var min int64 = MaxInt64
	var min2 int64 = MaxInt64
	var minKey string
	for key, a := range row {
		if a == -1 {
			continue
		}
		if a < min {
			min = a
			minKey = key
			continue
		}
	}

	for key, a := range row {
		if a == -1 || key == minKey {
			continue
		}
		if a < min2 {
			min2 = a
			continue
		}
	}

	return min, min2
}

func minimum(row map[string]int64) int64 {
	var min int64 = MaxInt64
	for _, a := range row {
		if a == -1 {
			continue
		}
		if a < min {
			min = a
			continue
		}
	}
	return min
}

func (n *Node) minimumWithBlocked(row map[string]int64, blocked map[string]bool) int64 {
	var min int64 = MaxInt64
	for loc, a := range row {
		if a == -1 || blocked[loc] {
			continue
		}
		if a < min {
			min = a
			continue
		}
	}
	if min == MaxInt64 {
		return 0
	}
	return min
}
