package branchandbound

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNode_CalculateLowerBound_Simple(t *testing.T) {
	var(
		assert = assert.New(t)
		cost = map[string]map[string]int64{
			"0": {"0": -1, "1": 10, "2": 15, "3": 20},
		}
		node = NewNode(cost, nil, []string{"0"}, ComplexLowerBound)
	)
	node.Calc()
	assert.Equal(12.5, node.LowerBound)
}

func TestNode_CalculateLowerBound_Complex(t *testing.T) {
	var(
		assert = assert.New(t)
		cost = map[string]map[string]int64{
			"0": {"0": -1, "1": 10, "2": 15, "3": 20},
			"1": {"0": 10, "1": -1, "2": 35, "3": 25},
		}
		node = NewNode(cost, nil, []string{"0"}, ComplexLowerBound)
	)
	node.Calc()
	assert.Equal(30.0, node.LowerBound)
}