package branchandbound

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jonas612/go-branch-and-bound/samples"
)

const numThreads = 5

func TestTSP_Solve_Serial_Example4(t *testing.T) {
	var (
		assert = assert.New(t)
		tsp    *TSP
	)

	tsp = NewTSP(samples.Example4, "0", numThreads, false)
	tsp.Solve(ModeSerial, ComplexLowerBound)

	assert.Equal(int64(80), tsp.GetLowestCost())
	assert.Equal(80.0, tsp.GetLowestBound())

	// non deterministic parallelism can lead to different correct solutions
	// []string{"0", "2", "3", "1", "0"} || []string{"0", "1", "3", "2", "0"}
	assert.Equal("0", tsp.GetBestTour()[0])
	assert.Equal("3", tsp.GetBestTour()[2])
	assert.Equal("0", tsp.GetBestTour()[4])
}

func TestTSP_Solve_Parallel_Example4(t *testing.T) {
	var (
		assert = assert.New(t)
		tsp    *TSP
	)

	tsp = NewTSP(samples.Example4, "0", numThreads, false)
	tsp.Solve(ModeParallel, ComplexLowerBound)

	assert.Equal(int64(80), tsp.GetLowestCost())
	assert.Equal(80.0, tsp.GetLowestBound())

	// non deterministic parallelism can lead to different correct solutions
	// []string{"0", "2", "3", "1", "0"} || []string{"0", "1", "3", "2", "0"}
	assert.Equal("0", tsp.GetBestTour()[0])
	assert.Equal("3", tsp.GetBestTour()[2])
	assert.Equal("0", tsp.GetBestTour()[4])
}

func TestTSP_Solve_Serial_Example05(t *testing.T) {
	var (
		assert = assert.New(t)
		tsp    *TSP
	)

	tsp = NewTSP(samples.Example5, "1", numThreads, false)
	tsp.Solve(ModeSerial, ComplexLowerBound)

	assert.Equal(int64(28), tsp.GetLowestCost())
	assert.Equal(28.0, tsp.GetLowestBound())
	assert.Equal([]string{"1", "4", "2", "5", "3", "1"}, tsp.GetBestTour())
}

func TestTSP_Solve_Parallel_Example05(t *testing.T) {
	var (
		assert = assert.New(t)
		tsp    *TSP
	)

	tsp = NewTSP(samples.Example5, "1", numThreads, false)
	tsp.Solve(ModeSerial, ComplexLowerBound)

	assert.Equal(int64(28), tsp.GetLowestCost())
	assert.Equal(28.0, tsp.GetLowestBound())
	assert.Equal([]string{"1", "4", "2", "5", "3", "1"}, tsp.GetBestTour())
}

// Benchmarks
// Configuration
var boundMode = ComplexLowerBound
var result int64 // to avoid compiler optimization

// Commands
// go test -bench=Standard -benchtime=10s -run=^$
// go test -bench=Cities -benchtime=10s -run=^$

// benchmarkTSPSolve is a wrapper for given examples
func benchmarkTSPSolve(example map[string]map[string]int64, startNode string, mode int, threadCount int, b *testing.B) {
	var (
		tsp *TSP
		r   int64
	)

	tsp = NewTSP(example, startNode, threadCount, false)

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		r = tsp.Solve(mode, boundMode)
	}
	result = r
}

// Benchmarks
func BenchmarkTSP_Solve_S_1_4_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example4, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_4_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example4, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_3_4_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example4, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_5_4_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example4, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_7_4_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example4, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_10_4_Standard(b *testing.B) { benchmarkTSPSolve(samples.Example4, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_5_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example5, "1", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_5_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example5, "1", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_3_5_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example5, "1", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_5_5_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example5, "1", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_7_5_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example5, "1", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_10_5_Standard(b *testing.B) { benchmarkTSPSolve(samples.Example5, "1", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_10_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example10, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_10_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example10, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_3_10_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example10, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_5_10_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example10, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_7_10_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example10, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_10_10_Standard(b *testing.B) { benchmarkTSPSolve(samples.Example10, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_15_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example15, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_15_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example15, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_3_15_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example15, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_5_15_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example15, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_7_15_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example15, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_10_15_Standard(b *testing.B) { benchmarkTSPSolve(samples.Example15, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_20_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example20, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_20_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example20, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_3_20_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example20, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_5_20_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example20, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_7_20_Standard(b *testing.B)  { benchmarkTSPSolve(samples.Example20, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_10_20_Standard(b *testing.B) { benchmarkTSPSolve(samples.Example20, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_16Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_16Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities16, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_16CitiesAlt1(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt1, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_16CitiesAlt2(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt2, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_16CitiesAlt3(b *testing.B)  { benchmarkTSPSolve(samples.Cities16Alt3, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_16CitiesAlt4(b *testing.B) { benchmarkTSPSolve(samples.Cities16Alt4, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_20Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_20Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities20, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_22Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_22Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities22, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_24Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities24, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_26Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities26, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_28Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_28Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities28, "0", ModeParallel, 10, b) }

func BenchmarkTSP_Solve_S_1_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeSerial, 1, b) }
func BenchmarkTSP_Solve_P_1_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 1, b) }
func BenchmarkTSP_Solve_P_2_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 2, b) }
func BenchmarkTSP_Solve_P_3_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 3, b) }
func BenchmarkTSP_Solve_P_4_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 4, b) }
func BenchmarkTSP_Solve_P_5_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 5, b) }
func BenchmarkTSP_Solve_P_6_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 6, b) }
func BenchmarkTSP_Solve_P_7_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 7, b) }
func BenchmarkTSP_Solve_P_8_30Cities(b *testing.B) { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 8, b) }
func BenchmarkTSP_Solve_P_9_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 9, b) }
func BenchmarkTSP_Solve_P_10_30Cities(b *testing.B)  { benchmarkTSPSolve(samples.Cities30, "0", ModeParallel, 10, b) }
