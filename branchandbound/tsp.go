package branchandbound

import (
	"fmt"
	"sync"
)

const maxInt64 = 1<<63 - 1

const (
	// ModeSerial is the configuration for serial run
	ModeSerial = 0
	// ModeParallel is the configuration for parallel run
	ModeParallel = 1
)

// TSP represents one instance of a TSP problem
type TSP struct {
	// parameters
	costMatrix  map[string]map[string]int64
	startNode   string
	threadCount int
	printLogs   bool

	// queue
	mu               sync.Mutex
	threads          []*Thread
	threadNodeCounts map[int]int
	runningThreads   int

	// results
	lowestBound float64
	lowestCost  int64
	bestTour    []string

	// metric
	exploredNodes int64
}

// NewTSP returns a new TSP instance
func NewTSP(costMatrix map[string]map[string]int64, startNode string, threadCount int, printLogs bool) *TSP {
	t := TSP{}
	t.costMatrix = costMatrix
	t.startNode = startNode
	t.printLogs = printLogs
	t.threadCount = threadCount

	t.threads = make([]*Thread, threadCount)

	t.lowestBound = maxInt64
	t.lowestCost = maxInt64
	t.bestTour = make([]string, len(costMatrix))

	t.exploredNodes = 0
	t.threadNodeCounts = map[int]int{}

	return &t
}

func (t *TSP) reset() {
	t.threads = make([]*Thread, t.threadCount)

	t.lowestBound = maxInt64
	t.lowestCost = maxInt64
	t.bestTour = make([]string, len(t.costMatrix))

	t.exploredNodes = 0
}

// Solve solves the TSP using branch and bound
func (t *TSP) Solve(mode int, boundMode int) int64 {
	t.reset() // safety in case Solve() has been called earlier

	root := NewNode(t.costMatrix, nil, []string{t.startNode}, boundMode)

	switch mode {
	case ModeSerial:
		t.solveSerial(root)
	case ModeParallel:
		t.solveParallel(root)
	default:
		panic("[TSP] Unknown mode!")
	}

	return t.lowestCost
}

// solveSerial solves the TSP using serial depth-first recursion
func (t *TSP) solveSerial(node *Node) {
	t.exploredNodes++
	node.Calc()

	if t.printLogs {
		fmt.Println(node.ToString())
	}

	if node.LowerBound > t.lowestBound {
		return
	}

	if node.Level() == len(t.costMatrix)-1 {
		if t.costMatrix[node.Tour[len(node.Tour)-1]][node.Tour[0]] != -1 {
			finalNode := NewNode(t.costMatrix, node, append(node.Tour, node.Tour[0]), node.boundMode)
			finalNode.Calc()
			if t.printLogs {
				fmt.Println(finalNode.ToString())
			}

			if finalNode.LowerBound >= t.lowestBound {
				return
			}
			t.bestTour = finalNode.Tour
			t.lowestBound = finalNode.LowerBound
			t.lowestCost = finalNode.TotalCost

			if t.printLogs {
				fmt.Printf("[INFO] Global lowest bound updated to: %f\n", t.lowestBound)
				fmt.Printf("[INFO] Global best tour updated to: %v\n", t.bestTour)
				fmt.Printf("[INFO] Global best cost updated to: %d\n", finalNode.TotalCost)
			}
			return
		}
	}

	for _, n := range node.CreateChildren() {
		t.solveSerial(n)
	}

}

// solveParallel solves the TSP using parallel workers with distributed queues
func (t *TSP) solveParallel(root *Node) {
	if t.printLogs {
		fmt.Printf("[TSP] Start caclulation with %d threads\n", t.threadCount)
	}

	root.Calc()

	if t.printLogs {
		fmt.Printf("[TSP] Root node created %s\n", root.ToString())
	}

	done := make(chan int)
	t.runningThreads = 0

	// create channels
	requestChs := make([]chan int, t.threadCount)
	nodeChs := make([]chan []*Node, t.threadCount)
	for i := 0; i < t.threadCount; i++ {
		requestChs[i] = make(chan int, 1)
		nodeChs[i] = make(chan []*Node)
	}

	// create threads
	var lastThreadID = 99
	for i := 0; i < t.threadCount; i++ {
		q := NewParallelQueue()
		stopCh := make(chan int)
		thread := NewThread(i, t, q, lastThreadID, done, stopCh, requestChs, nodeChs, t.printLogs)
		t.threads[i] = thread
		lastThreadID = thread.id
	}
	t.threads[0].SetNeighborID(lastThreadID) // close the circle

	// enqueue nodes in random order; range is non-deterministic
	workerID := 0
	for _, child := range root.CreateChildren() {
		child.Calc()
		t.threads[workerID%t.threadCount].AddNodeToQueue(child)
		workerID++
	}

	// print initial distribution
	if t.printLogs {
		for i := 0; i < t.threadCount; i++ {
			fmt.Printf("[THREAD][%d] Queue size %d\n", i, t.threads[i].QueueSize())
		}
	}

	// start execution
	for _, thread := range t.threads {
		go thread.Execute()
		t.runningThreads++
	}

	var quit bool
	for {
		select {
		case msg, ok := <-done:
			if !ok {
				break
			}
			t.runningThreads += msg

			if t.runningThreads == 0 {
				if t.printLogs {
					fmt.Println("[TSP] All workers idle, terminating")
				}
				quit = true

				for _, thread := range t.threads {
					thread.stopCh <- 1
					nodeCount := <-thread.stopCh
					t.threadNodeCounts[thread.id] = nodeCount
					t.exploredNodes += int64(nodeCount)
					if t.printLogs {
						fmt.Printf("[TSP] Thread %d explored %d nodes\n", thread.id, nodeCount)
					}
					close(thread.stopCh)
				}
				for i := 0; i < t.threadCount; i++ {
					close(requestChs[i])
					close(nodeChs[i])
				}
				close(done)

				break
			}
		}
		if quit {
			break
		}
	}
}

// SetBestSolution sets a new best solution
func (t *TSP) SetBestSolution(lb float64, cost int64, tour []string) {
	t.mu.Lock()
	// check if new solution is really better (possible race condition)
	if t.lowestCost >= cost {
		t.lowestBound = lb
		t.lowestCost = cost
		t.bestTour = tour
	}
	t.mu.Unlock()
}

// GetLowestBound returns the current lowest bound
func (t *TSP) GetLowestBound() float64 {
	// dirty reads are no problem here, because in the worst case a worker
	// spends a single iteration on a node that could have been pruned
	return t.lowestBound
}

// GetLowestCost returns the current lowest costs
func (t *TSP) GetLowestCost() int64 {
	t.mu.Lock()
	defer t.mu.Unlock()
	return t.lowestCost
}

// GetNodesExplored returns the current number of nodes explored
func (t *TSP) GetNodesExplored() int64 {
	t.mu.Lock()
	defer t.mu.Unlock()
	return t.exploredNodes
}

// GetNodesExploredPerThread returns the current number of nodes explored per thread
func (t *TSP) GetNodesExploredPerThread() map[int]int {
	t.mu.Lock()
	defer t.mu.Unlock()
	return t.threadNodeCounts
}

// GetBestTour returns the current best tour
func (t *TSP) GetBestTour() []string {
	t.mu.Lock()
	defer t.mu.Unlock()
	return t.bestTour
}
