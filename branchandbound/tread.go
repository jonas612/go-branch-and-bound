package branchandbound

import (
	"fmt"
)

// BalancingModeNone represents a configuration with no load balancing
const BalancingModeNone = 0

// BalancingModeSimple represents a configuration with load balancing a single node
const BalancingModeSimple = 1

// BalancingModeAdvanced represents a configuration with load balancing a split of the queue
const BalancingModeAdvanced = 2

// Configuration for the load balancing
var balancingMode = BalancingModeAdvanced

// Thread is a worker to process Nodes
type Thread struct {
	id                     int
	tsp                    *TSP
	queue                  *ParallelQueue
	idle                   bool
	debug                  bool
	nodeCount              int
	neighborID             int
	statusCh               chan int
	stopCh                 chan int
	reqChs                 []chan int
	nodeChs                []chan []*Node
	pendingSendRequestFrom int
}

// NewThread returns a new Thread
func NewThread(id int, tsp *TSP, queue *ParallelQueue, neighborID int, statusCh chan int, stopCh chan int, reqCh []chan int, nodeCh []chan []*Node, debug bool) *Thread {
	t := Thread{}
	t.id = id
	t.tsp = tsp
	t.queue = queue
	t.idle = false
	t.debug = debug
	t.nodeCount = 0
	t.neighborID = neighborID
	t.statusCh = statusCh
	t.stopCh = stopCh
	t.reqChs = reqCh
	t.nodeChs = nodeCh
	t.pendingSendRequestFrom = -1

	return &t
}

// Execute starts the worker (should be called asynchronously)
func (t *Thread) Execute() {
	if t.debug {
		fmt.Printf("[THREAD][%d] Thread started; Neighbor: %d\n", t.id, t.neighborID)
	}

	// this for runs until the thread is stopped
	for {
		var node *Node
		var ok bool

		// REQUEST Phase
		if node = t.queue.First(); node == nil {
			t.idle = true

			// edge case: 1 thread | termination om empty queue
			if t.id == t.neighborID {
				t.statusCh <- -1
				<-t.stopCh
				t.stopCh <- t.nodeCount
				return
			}

			if t.debug {
				fmt.Printf("[THREAD][%d] Thread idle\n", t.id)
				fmt.Printf("[THREAD][%d] Out of work, asking %d for work\n", t.id, t.neighborID)
			}

			// ask for work
			if !(balancingMode == BalancingModeNone) {
				t.reqChs[t.neighborID] <- t.id
			}

			// communicate idle state
			t.statusCh <- -1

			if t.debug {
				fmt.Printf("[THREAD][%d] Asked %d for work, waiting ...\n", t.id, t.neighborID)
			}

			var nodes []*Node
			select {
			case nodes, ok = <-t.nodeChs[t.id]:
				// listen to own node channel
				if !ok {
					break
				}
				if nodes == nil || len(nodes) == 0 {
					if t.debug {
						fmt.Printf("[THREAD][%d] Received nothing from %d \n", t.id, t.neighborID)
					}
					break
				}
				// enqueue nodes and continue
				for _, newNode := range nodes {
					t.queue.Add(newNode)
					if t.debug {
						fmt.Printf("[THREAD][%d] Received %s from %d, resuming\n", t.id, newNode.ToStringID(), t.neighborID)
					}
				}
				node = t.queue.First()

			case <-t.stopCh:
				// listen stop channel
				if t.debug {
					fmt.Printf("[THREAD][%d] Closing...\n", t.id)
				}
				t.stopCh <- t.nodeCount
				if t.debug {
					fmt.Printf("[THREAD][%d] Closed\n", t.id)
				}
				return
			}
		}

		// safety
		if node == nil {
			continue
		}

		// RESPONSE Phase
		select {
		case i := <-t.reqChs[t.id]:

			// log pending request
			if t.debug {
				fmt.Printf("[THREAD][%d] Cached request from %d\n", t.id, i)
			}
			t.pendingSendRequestFrom = i
		default:

		}

		if !(balancingMode == BalancingModeNone) {
			// every 10th run, check if request is pending and try to satisfy

			if t.pendingSendRequestFrom != -1 && t.nodeCount%1 == 0 && t.queue.Size() >= 10 {
				// sent first node to sender if possible
				var nodesToSend []*Node

				// try to find a node with an appropriate bound
				if balancingMode == BalancingModeAdvanced {
					nodesToSend = t.queue.Split()
				} else {
					nodesToSend = []*Node{t.queue.First()}
				}

				if nodesToSend == nil || len(nodesToSend) == 0 {
					if t.debug {
						fmt.Printf("[THREAD][%d] Fulfil request from %d; empty queue\n", t.id, t.pendingSendRequestFrom)
					}
				} else {
					if t.debug {
						fmt.Printf("[THREAD][%d] Fulfil request from %d; sending %v\n", t.id, t.pendingSendRequestFrom, nodesToSend)
					}
					t.statusCh <- 1
					t.nodeChs[t.pendingSendRequestFrom] <- nodesToSend
					t.pendingSendRequestFrom = -1
				}
			}
		}

		// EXPLORATION Phase
		t.idle = false
		t.nodeCount++

		if t.debug {
			fmt.Printf("[THREAD][%d] Explore node (total: %d) %v\n", t.id, t.nodeCount, node.ToString())
		}

		// for safety if the global bound has been updated in the meantime, normally those nodes should not be enqueued
		if node.LowerBound > t.tsp.GetLowestBound() {
			if t.debug {
				fmt.Printf("[THREAD][%d] Dropping node (total: %d) %v\n", t.id, t.nodeCount, node.ToString())
			}
			continue
		}

		if node.Level() < len(t.tsp.costMatrix)-1 {
			for _, n := range node.CreateChildren() {
				n.Calc()
				if n.LowerBound <= t.tsp.GetLowestBound() {
					t.queue.Add(n)
				} else {
					if t.debug {
						fmt.Printf("[THREAD][%d] Dropping node (total: %d) %v\n", t.id, t.nodeCount, n.ToString())
					}
				}
			}
			continue
		}

		if node.Level() == len(t.tsp.costMatrix)-1 {
			if t.tsp.costMatrix[node.Tour[len(node.Tour)-1]][node.Tour[0]] != -1 {
				finalNode := NewNode(t.tsp.costMatrix, node, append(node.Tour, node.Tour[0]), node.boundMode)
				finalNode.Calc()
				if finalNode.LowerBound <= t.tsp.GetLowestBound() {
					t.queue.Add(finalNode)
				} else {
					if t.debug {
						fmt.Printf("[THREAD][%d] Dropping node (total: %d) %v\n", t.id, t.nodeCount, finalNode.ToString())
					}
				}
			}
			continue
		}
		if node.Level() == len(t.tsp.costMatrix) {
			if node.LowerBound >= t.tsp.GetLowestBound() {
				if t.debug {
					fmt.Printf("[THREAD][%d] Dropping node (total: %d) %v\n", t.id, t.nodeCount, node.ToString())
				}
				continue
			}
			t.tsp.SetBestSolution(node.LowerBound, node.TotalCost, node.Tour)

			if t.debug {
				fmt.Printf("[THREAD][%d] Global lowest bound updated to: %f\n", t.id, node.LowerBound)
				fmt.Printf("[THREAD][%d] Global best tour updated to: %v\n", t.id, node.TotalCost)
				fmt.Printf("[THREAD][%d] Global best cost updated to: %d\n", t.id, node.TotalCost)
			}
			continue
		}
		panic("This should not be reached.")
	}
}

// GetID returns the ID of a Thread
func (t *Thread) GetID() int {
	return t.id
}

// IsIdle determines if the thread is currently idle
func (t *Thread) IsIdle() bool {
	return t.idle
}

// SetQueue sets a ParallelQueue for the Thread
func (t *Thread) SetQueue(q *ParallelQueue) {
	t.queue = q
}

// SetNeighborID sets the neighbor id of the thread
func (t *Thread) SetNeighborID(n int) {
	t.neighborID = n
}

// AddNodeToQueue add a Node to a workers ParallelQueue
func (t *Thread) AddNodeToQueue(n *Node) {
	t.queue.Add(n)
}

// QueueSize return the current size of a workers ParallelQueue
func (t *Thread) QueueSize() int {
	return t.queue.Size()
}
