package branchandbound

import (
	"container/heap"
	"sync"
)

// ParallelQueue manages the access to and configuration of a heap queue for nodes
type ParallelQueue struct {
	q       *queue
	mu      *sync.Mutex
}

// NewParallelQueue returns s new ParallelQueue
func NewParallelQueue() *ParallelQueue {
	pq := ParallelQueue{}
	pq.mu = &sync.Mutex{}
	pq.q = newQueue()
	return &pq
}

type queue []*Node

func newQueue() *queue {
	q := make(queue, 0)
	heap.Init(&q)
	return &q
}

func (q queue) Len() int { return len(q) }

func (q queue) size() int { return len(q) }

func (q queue) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority so we use greater than here.
	// return q[i].priority > q[j].priority
	node1 := q[i]
	node2 := q[j]

	if node1 == nil || node2 == nil {
		return false
	}

	if node1.Level() < node2.Level() {
		return false
	}
	if node1.Level() > node2.Level() {
		return true
	}

	// level is identical, checking bound
	if node1.LowerBound < node2.LowerBound {
		return true
	}
	if node1.LowerBound > node2.LowerBound {
		return false
	}

	// bound is identical, checking total cost
	if node1.currentTotalCost() < node2.currentTotalCost() {
		return true
	}
	if node1.currentTotalCost() > node2.currentTotalCost() {
		return false
	}

	// the nodes are identical, order is irrelevant
	return false
}

func (q queue) Swap(i, j int) {
	if q.Len() < 1 {
		return
	}
	if i >= q.Len() || j >= q.Len() {
		return
	}
	q[i], q[j] = q[j], q[i]
}

func (q *queue) Push(x interface{}) {
	node := x.(*Node)
	*q = append(*q, node)
}

func (q *queue) Pop() interface{} {
	if q.size() == 0 {
		return nil
	}
	old := *q
	n := len(old)
	node := old[n-1]
	*q = old[0 : n-1]
	return node
}

// Add adds a node to the queue while setting a mutex for parallel access
func (pq *ParallelQueue) Add(node *Node) {
	if node == nil {
		return
	}
	if !node.Calculated {
		panic("Node not calculated!")
	}

	pq.mu.Lock()
	if pq.q.Len() < 1 {
		*pq.q = append(*pq.q, node)
	} else {
		heap.Push(pq.q, node)
	}
	pq.mu.Unlock()
}

// First return the highest priority item from the queue while setting a mutex for parallel access
func (pq *ParallelQueue) First() *Node {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	var node *Node
	pop := heap.Pop(pq.q)
	if pop != nil {
		node = pop.(*Node)
	}
	return node
}

// Last return the lowest priority item from the queue while setting a mutex for parallel access
func (pq *ParallelQueue) Last() *Node {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	if pq.q.size() < 1 {
		return nil
	}

	var node *Node
	last := heap.Remove(pq.q, pq.q.size()-1)
	if last != nil {
		node = last.(*Node)
	}
	return node
}

// Split return every 2nd element of the queue
func (pq *ParallelQueue) Split() []*Node {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	if pq.q.size() < 1 {
		return nil
	}

	var nodes = []*Node{}
	var halfSize = pq.q.size() / 2
	var pointer = 1
	for pointer < halfSize {
		nodeToBeRemoved := heap.Remove(pq.q, pointer)
		var node *Node
		if nodeToBeRemoved != nil {
			node = nodeToBeRemoved.(*Node)
		}
		nodes = append(nodes, node)
		pointer++ // takes the 2nd next element, because one has been removed
	}
	return nodes
}

// Size returns the current size of the ParallelQueue
func (pq *ParallelQueue) Size() int {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	return pq.q.size()
}
